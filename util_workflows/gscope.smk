

reads = config["reads"]
if(isinstance(reads, str)):
    reads = [reads]
outbase = config["outbase"]
ploidy = config.get("ploidy", 2)


rule genomescope:
    input:
        reads = reads
    output:
        tar=outbase + ".gscope2.tar.gz",
        summary=outbase + ".gscope2.summary.txt"
    params:
        kmer = 21,
        fastqs = [r[:-3] for r in reads],
        base = outbase + ".gscope2",
        ploid = ploidy
    conda:
        "readqc.yml"
    threads:
        4
    shell:
        """
        gunzip {input}
        jellyfish count -C -m {params.kmer} -s 1000000000 -t {threads} {params.fastqs} -o reads.jf
        jellyfish histo -t {threads} reads.jf > reads.histo
        mkdir -p $(dirname {params.base})
        genomescope2 -p {params.ploid} -i reads.histo -o {params.base} -k {params.kmer}
        mv reads.histo {params.base}
        tar -zcf {output.tar} {params.base}
        mv {params.base}/summary.txt {output.summary}
        """

