

assembly = config["assembly"]
taxid = config["taxid"]
outdir = config["outdir"]
base = assembly[:-1 * len(".fasta")]

rule fcs_gx:
    input:
        assembly
    output:
        fcs_report = f"{outdir}/{base}.{taxid}.fcs_gx_report.txt",
        fcs_rpt = f"{outdir}/{base}.{taxid}.taxonomy.rpt",
        clean_fasta = f"{outdir}/clean.fasta",
        contam_fasta = f"{outdir}/contam.fasta",
        fcs_log = f"{outdir}/{base}.{taxid}.fcs_gx.log"
    params:
        taxid = taxid,
        outdir = outdir,
        base = base
    conda:
        "fcs.yml"
    threads:
        config["threads"]
    shell:
        """
        mkdir tmpfs
        # mount -t tmpfs tmpfs tmpfs -o size=470G
        s5cmd  --no-sign-request cp  --part-size 50  --concurrency 50 s3://ncbi-fcs-gx/gxdb/latest/all.* tmpfs/fcs_gx_db/
        curl https://ftp.ncbi.nlm.nih.gov/genomes/TOOLS/FCS/releases/latest/fcs-gx.sif -Lo fcs-gx.sif
        curl -LO https://github.com/ncbi/fcs/raw/main/dist/fcs.py
        python fcs.py --image fcs-gx.sif screen  genome \\
            --fasta {input} \\
            --out-dir {params.outdir} \\
            --gx-db tmpfs/fcs_gx_db/ \\
            --tax-id {params.taxid} \\
            > fcs.log

        cat {input} | python fcs.py --image fcs-gx.sif clean genome \\
            --action-report {output.fcs_report} \\
            --output {output.clean_fasta} \\
            --contam-fasta-out {output.contam_fasta} \\
            >> fcs.log
        mv fcs.log {output.fcs_log}
        """
