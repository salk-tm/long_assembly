

rule helixer:
    input:
        config["reference"]
    output:
        config["output"]
    params:
        model = config["model"]
    container:
        "docker://gglyptodon/helixer-docker:helixer_v0.3.2_cuda_11.8.0-cudnn8"
    threads:
        config["cpu"]
    shell:
        """
        fetch_helixer_models.py -l {params.model}
        Helixer.py --fasta-path {input} --gff-output-path {output} --lineage {params.model}
        """
