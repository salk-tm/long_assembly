#!/usr/bin/env python3
import argparse
import enum
import subprocess
import pandas
import os
from io import StringIO
from datetime import datetime
import sys

from misc_structs import busco_datasets, performance_profiles
from rds_tools import insert_data

workflow_version = "V0.5.1"

jobinfo = {
    "miniasm": {
        "help": "use ont reads to run miniasm, polish with racon, polish with pilon if sr1 and sr2",
        "needs": ["ont"],
        "polish": True,
        "suffixes": ["miniasm"]
    },
    "flye_HQ": {
        "help": "use ont reads to run flye, polish with racon, then polish with pilon if sr1 and sr2",
        "needs": ["ont"],
        "polish": True,
        "suffixes": ["flye_HQ"]
    },
    "flye_HIFI": {
        "help": "use hifi reads to run flye",
        "needs": ["hifi"],
        "polish": False,
        "suffixes": ["flye_HIFI"]
    },
    "wtdbg2": {
        "help": "use ont reads to run wtdbg2, polish with racon, then polish with pilon if sr1 and sr2",
        "needs": ["ont", "genome_size"],
        "polish": True,
        "suffixes": ["wtdbg2"]
    },
    "spades": {
        "help": "use spades to assemble sr1 and sr2",
        "needs": ["sr1", "sr2"],
        "polish": False,
        "suffixes": ["spades"]
    },
    "hifiasm": {
        "help": "use hifi reads to run hifiasm",
        "needs": ["hifi"],
        "polish": False,
        "suffixes": ["hifiasm", "hifiasm.hap1", "hifiasm.hap2"]
    },
    "hifiasm_hic": {
        "help": "use hifi reads and hic reads to run hifiasm",
        "needs": ["hifi", "hic1", "hic2"],
        "polish": False,
        "suffixes": ["hifiasm_hic", "hifiasm_hic.hap1", "hifiasm_hic.hap2"]
    },
    "hifiasm_trio": {
        "help": "use hifi reads and parental reads to run hifiasm in trio mode",
        "needs": ["hifi", "par1", "par2"],
        "polish": False,
        "suffixes": ["hifiasm_trio.hap1", "hifiasm_trio.hap2"]
    },
    "hifiasm_ulr_trio": {
        "help": "use hifi, ont, and parental reads to run hifiasm in trio mode with ultra-long input",
        "needs": ["hifi", "par1", "par2", "ont"],
        "polish": False,
        "suffixes": ["hifiasm_ulr_trio.hap1", "hifiasm_ulr_trio.hap2"]
    },
    "hifiasm_meta": {
        "help": "use hifi reads to run hifiasm_meta",
        "needs": ["hifi"],
        "polish": False,
        "suffixes": ["hifiasm_meta"]
    },
    "hifiasm_ulr": {
        "help": "use hifi and ont reads to run hifiasm with ultra-long input",
        "needs": ["hifi", "ont"],
        "polish": False,
        "suffixes": ["hifiasm_ulr", "hifiasm_ulr.hap1", "hifiasm_ulr.hap2"]
    },
    "hifiasm_ulr_hic": {
        "help": "use hifi, ont, and hic reads to run hifiasm with ultra-long input and hic haplotyping",
        "needs": ["hifi", "ont", "hic1", "hic2"],
        "polish": False,
        "suffixes": ["hifiasm_ulr_hic", "hifiasm_ulr_hic.hap1", "hifiasm_ulr_hic.hap2"]
    },
    "ipa": {
        "help": "use hifi reads to run IPA",
        "needs": ["hifi"],
        "polish": False,
        "suffixes": ["ipa_p_ctg"]
    },
    "verkko": {
        "help": "use hifi and ont reads to run Verkko",
        "needs": ["hifi", "ont"],
        "polish": False,
        "suffixes": ["verkko"]
    },
    "verkko_hic": {
        "help": "use hifi, ont, and hic reads to run verkko with ultra-long input and hic haplotyping",
        "needs": ["hifi", "ont", "hic1", "hic2"],
        "polish": False,
        "suffixes": ["verkko_hic", "verkko_hic.hap1", "verkko_hic.hap2"]
    }
}

cmd_template = """snakemake -p --use-conda --use-singularity --tibanna -j {jobs} \\
    {targets} \\
    --default-remote-prefix {s3pre} \\
    --default-resources disk_mb=4000000 \\
    --tibanna-config root_ebs_size=32 log_bucket={log_bucket} \\
    --config \\
        {conf}"""

snakedir = os.path.join(os.path.dirname(__file__))


def s3open(s3uri):
    cmd = f"aws s3 cp {s3uri} -"
    proc = subprocess.run(cmd, capture_output=True, shell=True)
    return StringIO(proc.stdout.decode("utf-8"))

def ont_stats(path):
    df = pandas.read_html(s3open(path))[0]
    return {
        "ont_n50": int(float(df.iloc[6, 1])),
        "ont_nbases": int(float(df.iloc[8, 1])),
        "ont_meanQ": float(df.iloc[2, 1])
    }

def hifi_stats(path):
    df = pandas.read_html(s3open(path))[0]
    return {
        "hifi_n50": int(float(df.iloc[6, 1])),
        "hifi_nbases": int(float(df.iloc[8, 1])),
        "hifi_meanQ": float(df.iloc[2, 1])
    }

def sr_stats(path1, path2, gscope):
    df1 = pandas.read_html(s3open(path1))[0]
    df2 = pandas.read_html(s3open(path2))[0]
    df3 = pandas.read_csv(s3open(gscope), skiprows=6, sep="\s\s+", engine="python")
    bases1 = int(float(df1.iloc[3, 1])) * int(float(df1.iloc[5, 1].split("-")[-1]))
    bases2 = int(float(df2.iloc[3, 1])) * int(float(df2.iloc[5, 1].split("-")[-1]))
    bptype = lambda s: -1 if s == "Inf bp" else int(float(s[:-3].replace(",", "")))
    perctype = lambda s: float(s[:-1]) / 100
    gscope_cols = [
        "homozygous", "heterozygous", "haploid_len",
        "repeat_len", "unique_len", "model_fit", "read_eror"
    ]
    gscope_types = [perctype, perctype, bptype, bptype, bptype, perctype, perctype]
    ret = {
        "ill_nbases": bases1 + bases2,
    }
    for i, (c, t) in enumerate(zip(gscope_cols, gscope_types)):
        ret[f'gscope_min_{c}'] = t(df3.iloc[i, 1])
        ret[f'gscope_max_{c}'] = t(df3.iloc[i, 2])
    return ret

def assembly_stats(path):
    df = pandas.read_csv(s3open(path), sep="\t")
    keys = [
        "assembly_total_len", "assembly_N100n", "assembly_mean_len",
        "assembly_N0", "assembly_N100", "assembly_N_count",
        "assembly_gap_count", "assembly_N50", "assembly_N50n",
        "assembly_N70", "assembly_N70n",
        "assembly_N90", "assembly_N90n", 
    ]
    return {k: int(df.iloc[0, i + 1]) for i, k in enumerate(keys)}

def busco_stats(path):
    raw = s3open(path).read()
    raw = raw.split("C:")[1].split("\n")[1:7]
    counts = [int(l.split("\t")[1]) for l in raw]
    keys = ["busco_C", "busco_S", "busco_D", "busco_F", "busco_M", "busco_n"]
    return dict(zip(keys, counts))

def qv_stats(path):
    return float(s3open(path).read().split("\t")[3])


def get_args(jobopts):
    parser = argparse.ArgumentParser(
        description=(
            "automates submitting an assembly job through tibanna for some dataset"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    group_req = parser.add_argument_group("Required")
    group_req.add_argument(
        "s3prefix",
        help="an s3 prefix that all files should share and be interpretted relative to"
    )
    group_req.add_argument(
        "outprefix",
        help="a prefix used for naming output files"
    )
    group_req.add_argument(
        "version",
        help="version identifier for assembly used for naming output files"
    )
    group_req.add_argument(
        "busco",
        help="busco_db to use for evaluating assembly; Ex: liliopsida_odb10, eudicots_odb10"
    )
    group_req.add_argument(
        "taxid",
        help="NCBI taxonomic ID for your sample"
    )
    group_input = parser.add_argument_group("Input")
    group_input.add_argument(
        "--ont",
        nargs="*",
        default=[],
        help="s3 suffix path to ONT reads"
    )
    group_input.add_argument(
        "--hifi",
        nargs="*",
        default=[],
        help="s3 suffix path to hifi reads"
    )
    group_input.add_argument(
        "--sr1",
        help="s3 suffix path to R1 illumina reads"
    )
    group_input.add_argument(
        "--sr2",
        help="s3 suffix path to R2 illumina reads"
    )
    group_input.add_argument(
        "--hic1",
        help="s3 suffix path to R1 hic reads"
    )
    group_input.add_argument(
        "--hic2",
        help="s3 suffix path to R2 hic reads"
    )
    group_input.add_argument(
        "--par1",
        nargs="*",
        default=[],
        help="s3 suffix path to parent1 hifi reads"
    )
    group_input.add_argument(
        "--par2",
        nargs="*",
        default=[],
        help="s3 suffix path to parent2 hifi reads"
    )
    group_input.add_argument(
        "--genome_size",
        help="a genome size estimate"
    )
    group_input.add_argument(
        "--ont_subset",
        help="if present, subset ont reads before assembly"
    )
    group_input.add_argument(
        "--helixer_model",
        help="choice of gene prediction model. Defaults to land_plant",
        default="land_plant"
    )
    group_ass = parser.add_argument_group("Assemblers")
    for j, d in jobopts.items():
        group_ass.add_argument(f"--{j}", help=d['help'], action="store_true")
    group_ctrl = parser.add_argument_group("Controls")
    group_ctrl.add_argument(
        "--profile",
        choices=list(performance_profiles.keys()),
        help="controls what size compute nodes will be used",
        default="large"
    )
    group_ctrl.add_argument(
        "--jobs", "-j",
        help="controls how many compute nodes can be used",
        default=1,
        type=int
    )
    group_ctrl.add_argument(
        "--log_bucket", "-l",
        help="log bucket for tibanna",
        default="salk-tm-logs"
    )
    group_ctrl.add_argument(
        "--print", "-p", action="store_true",
        help="if present, only print the snakemake command and exit. (overrides dryrun)"
    )
    group_ctrl.add_argument(
        "--dryrun", "-d", action="store_true",
        help="if present, do a snakemake dryrun and exit"
    )
    group_ctrl.add_argument(
        "--configfile",
        help="configfile to use for opt configuration (must be in configs/)"
    )
    group_ctrl.add_argument(
        "--rds_json",
        help="path to json file containing configuration for rds table",
        default=os.path.expanduser("~/.aws/rds.json")
    )
    group_ctrl.add_argument(
        "--skipHelixer",
        action="store_true",
        help="if present, does not perform helixer annotation"
    )    
    args = parser.parse_args()
    d_args = vars(args)
    if(args.busco not in busco_datasets):
        raise ValueError(f"{args.busco} is not a valid Busco_DB")
    for j, d in jobopts.items():
        dtypes = d["needs"]
        if(d_args[j]):
            for d in dtypes:
                if(not d_args[d]):
                    raise ValueError(f"{j} requires {d} input")
    if(sum(d_args[j] for j in jobopts) == 0):
        sys.stderr.write("WARNING: No assemblies requested.\n")
    return args


def build_snakemake_command(args, jobopts, cmd_template):
    d_args = vars(args)
    targs_dict = {
        "buscos": [],
        "ass_stats": [],
        "merqury": [],
        "read_stats": {},
        "helixer": []
    }
    outbase = f"{args.outprefix}.{args.version}"
    for j, d in jobopts.items():
        if(not d_args[j]):
            continue
        for s in d["suffixes"]:
            temp = f"{outbase}.{s}"
            has_sr = args.sr1 is not None and args.sr2 is not None
            if(d['polish']):
                temp += ".racon3.pilon3" if(has_sr) else ".racon3"
            #if(has_sr):
            #    targs_dict['merqury'].append(os.path.join(args.s3prefix, temp + ".merqury.qv"))
            #targs_dict['buscos'].append(os.path.join(args.s3prefix, temp + f".busco_summary_{args.busco}.txt"))
            #targs_dict['ass_stats'].append(os.path.join(args.s3prefix, temp + f".assembly_stats.tsv"))
            temp += f".fcs_gx_{args.taxid}"
            if(has_sr):
                targs_dict['merqury'].append(os.path.join(args.s3prefix, temp + ".merqury.qv"))
            targs_dict['buscos'].append(os.path.join(args.s3prefix, temp + f".busco_summary_{args.busco}.txt"))
            targs_dict['ass_stats'].append(os.path.join(args.s3prefix, temp + f".assembly_stats.tsv"))
            targs_dict["helixer"].append(os.path.join(
                args.s3prefix,
                temp + f".helixer_{args.helixer_model}.buscoP_summary_{args.busco}.txt"
            ))
    if(len(args.ont) == 1 and args.ont_subset is None):
        targs_dict['read_stats']['ONT'] = os.path.join(args.s3prefix, f"{args.ont[0][:-9]}_nanoplot.html")
    if(len(args.ont) > 1 and args.ont_subset is None):
        targs_dict['read_stats']['ONT'] = os.path.join(args.s3prefix, f"{outbase}.ont.concat_nanoplot.html")
    if(len(args.ont) == 1 and args.ont_subset is not None):
        targs_dict['read_stats']['ONT'] = os.path.join(
            args.s3prefix,
            f"{args.ont[0][:-9]}.sub_{args.ont_subset}_nanoplot.html"
        )
    if(len(args.ont) > 1 and args.ont_subset is not None):
        targs_dict['read_stats']['ONT'] = os.path.join(
            args.s3prefix,
            f"{outbase}.ont.concat.sub_{args.ont_subset}_nanoplot.html"
        )
    if(len(args.hifi) == 1):
        targs_dict['read_stats']['HIFI'] = os.path.join(args.s3prefix, f"{args.hifi[0][:-9]}_nanoplot.html")
    if(len(args.hifi) > 1):
        targs_dict['read_stats']['HIFI'] = os.path.join(args.s3prefix, f"{outbase}.hifi.concat_nanoplot.html")
    if(args.sr1):
        targs_dict['read_stats']['SR1'] = os.path.join(args.s3prefix, f"{args.sr1[:-9]}_fastqc.html")
    if(args.sr2):
        targs_dict['read_stats']['SR2'] = os.path.join(args.s3prefix, f"{args.sr2[:-9]}_fastqc.html")
    if(args.sr1 and args.sr2):
        targs_dict['read_stats']['GSCOPE'] = os.path.join(
            args.s3prefix,
            f"{args.outprefix}.{args.version}.gscope2.summary.txt"
        )
    conf = {"basename": f"{args.outprefix}.{args.version}", "profile": args.profile}
    for k in ["sr1", "sr2", "genome_size", "ont_subset", "hic1", "hic2", "par1", "par2"]:
        if(d_args[k]):
            conf[k] = d_args[k]
    for k in ["ont", "hifi", "par1", "par2"]:
        if(len(d_args[k]) == 1):
            conf[k] = d_args[k][0]
        elif(len(d_args[k]) > 1):
            conf[k] = str(d_args[k]).replace(" ", "")
    conf = " \\\n        ".join(f"{k}={v}" for k, v in conf.items())
    targs = [
        t for c in ["buscos", "ass_stats", "merqury", "helixer"]
        for t in targs_dict[c]
    ]
    targs += list(targs_dict['read_stats'].values())
    targs = " \\\n    ".join(targs)
    cmd = cmd_template.format(
        jobs=args.jobs, targets=targs, s3pre=args.s3prefix, conf=conf, log_bucket=args.log_bucket
    )
    if(args.configfile):
        cmd += f" \\\n    --configfile {os.path.abspath(args.configfile)}"
    if(args.skipHelixer):
        cmd += " \\\n    --omit-from helixer"
    if(args.dryrun):
        cmd += " \\\n    --cores 64 --dryrun"
    return cmd, targs_dict


def collect_results(args, targs_dict):
    ret = []
    shared = {
        "path_ont": str(args.ont) if(args.ont) else None,
        "path_sr1": args.sr1 if(args.sr2) else None,
        "path_sr2": args.sr2 if(args.sr2) else None,
        "path_hifi": str(args.hifi) if(args.hifi) else None,
        "path_hic1": str(args.hic1) if(args.hic1) else None,
        "path_hic2": str(args.hic2) if(args.hic2) else None,
        "path_par1": str(args.par1) if(args.par1) else None,
        "path_par2": str(args.par2) if(args.par2) else None,
        "name_base": os.path.basename(args.outprefix),
        "version": args.version,
        "busco_db": args.busco,
        "workflow_version": workflow_version,
        "config_path": args.configfile,
        "s3prefix": args.s3prefix,
        "timestamp": datetime.now().timestamp()
    }
    if(args.ont):
        temp = ont_stats("s3://" + targs_dict['read_stats']['ONT'])
        shared = dict(**shared, **temp)
    if(args.hifi):
        temp = hifi_stats("s3://" + targs_dict['read_stats']['HIFI'])
        shared = dict(**shared, **temp)
    if(args.sr1 and args.sr2):
        temp = sr_stats(
            "s3://" + targs_dict['read_stats']['SR1'],
            "s3://" + targs_dict['read_stats']['SR2'],
            "s3://" + targs_dict['read_stats']['GSCOPE'],
        )
        shared = dict(**shared, **temp)
        qvs = [qv_stats("s3://" + f) for f in targs_dict['merqury']]
    else:
        qvs = [None] * len(targs_dict['buscos'])

    for b, s, qv in zip(targs_dict['buscos'], targs_dict['ass_stats'], qvs):
        curr = dict(path_assembly=s[:-19] + ".fasta", **shared)
        curr['name_assembly'] = os.path.basename(curr["path_assembly"])
        curr = dict(
            merqury_qv=qv,
            **curr,
            **busco_stats("s3://" + b),
            **assembly_stats("s3://" + s)
        )
        ret.append(curr)
    return ret


def cli_main(jobopts, cmd_template, snakedir):
    args = get_args(jobopts)
    cmd, targs_dict = build_snakemake_command(args, jobopts, cmd_template)
    if(args.print):
        print(cmd)
        return
    
    curdir = os.getcwd()
    os.chdir(snakedir)
    retcode = subprocess.call(cmd, shell=True)
    os.chdir(curdir)
    if(args.dryrun):
        return
    if(retcode != 0):
        raise ValueError(f"Snakemake execution halted with code : {retcode}\n" + str(args))

    results = collect_results(args, targs_dict)
    for d in results:
        insert_data(d, args.rds_json)


if(__name__ == "__main__"):
    cli_main(jobinfo, cmd_template, snakedir)

