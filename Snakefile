from misc_structs import performance_profiles, busco_datasets
from snakemake.utils import validate
import os

validate(config, "schema/config.schema.json")
for k in ["cpu_assembly", "cpu_polish", "mem_assembly", "mem_polish"]:
    config[k] = config.get(k, performance_profiles[config['profile']][k])
opts = config['opts']


ont_files = config.get("ont")
if(isinstance(ont_files, list)):
    config['ont'] = config['basename'] + ".ont.concat.fastq.gz"

hifi_files = config.get('hifi')
if(isinstance(hifi_files, list)):
    config['hifi'] = config['basename'] + ".hifi.concat.fastq.gz"


if("ont_subset" in config):
    config["ont"] = config["ont"].replace(".fastq.gz", ".sub_" + config["ont_subset"] + ".fastq.gz")


basesuffix = config.get('basename', "").split("/")[-1]


def gen_group(name, keys):
    return lambda wcs: ".".join([name] + [os.path.basename(wcs[k]) for k in keys])



if("basename" in config):
    rule concat_long_reads:
        input:
            lambda wcs: {'ont': ont_files, "hifi": hifi_files}[wcs['rt']]
        output:
            f"{config['basename']}.{{rt}}.concat.fastq.gz"
        threads:
            config['cpu_aux']
        resources:
            mem_mb=config['mem_aux']
        group:
            gen_group("concat", ["rt"])
        shell:
            """
            mkdir -p $(dirname {output})
            for f in {input}; do cat $f >> {output} ; done
            """


rule subset_fastq:
    input:
        "{base}.fastq.gz"
    output:
        "{base}.sub_{L}.fastq.gz"
    params:
        unzipped = lambda wcs: wcs['base'] + ".fastq"
    conda:
        "envs/bio_utils.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config['mem_aux']
    group:
        gen_group("subset", ["base"])
    shell:
        """
        gunzip {input}
        subset_fastq.py {params.unzipped} -l {wildcards.L} -k | gzip -c > {output}
        """


rule fastqc:
    input:
        "{prefix}.fastq.gz",
    output:
        "{prefix}_fastqc.html",
    conda:
        "envs/readqc.yml"
    params:
        base = lambda wcs: wcs["prefix"] + "_fastqc"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config['mem_aux']
    group:
        gen_group("fastqc", ["prefix"])
    shell:
        """
        fastqc {input}
        unzip {params.base}.zip -d $(dirname {params.base})
        """


rule nanoplot:
    input:
        "{prefix}.fastq.gz",
    output:
        "{prefix}_nanoplot.html"
    conda:
        "envs/readqc.yml"
    params:
        opt = config["opts"]["nanoplot"]
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config['mem_aux']
    group:
        gen_group("nanoplot", ["prefix"])
    shell:
        """
        NanoPlot -o {wildcards.prefix}_nanoplot -t {threads} --fastq {input} {params.opt}
        cp {wildcards.prefix}_nanoplot/NanoPlot-report.html {output}
        """

rule bam_to_hifi_fastq:
    input:
        "{base}.bam"
    output:
        "{base}.hifi.fastq.gz"
    conda:
        "envs/mini.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("bam2fq", ["base"])
    shell:
        """
        samtools fastq -@ {threads} {input} | bgzip -@ {threads} > {wildcards.base}.fastq.gz
        fastq-filter -q 20 -o {output} {wildcards.base}.fastq.gz
        """

rule bam_to_ccs_fastq:
    input:
        "{base}.bam"
    output:
        "{base}.ccs.fastq.gz"
    conda:
        "envs/mini.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("bam2fq", ["base"])
    shell:
        """
        samtools view -b -e '[rq]>-1' {input} | samtools fastq | bgzip -@ {threads} > {output}
        """

rule bam_to_fastq:
    input:
        "{base}.bam"
    output:
        "{base}.bam2fq.fastq.gz"
    conda:
        "envs/mini.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("bam2fq", ["base"])
    shell:
        """
        samtools fastq -@ {threads} {input} | bgzip -@ {threads} > {wildcards.base}.bam2fq.fastq.gz
        """

rule fastq_filt_Q_min_len:
    input:
        "{base}.fastq.gz"
    output:
        "{base}.Q{Q}.min_{L}k.fastq.gz"
    conda:
        "envs/mini.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("fastqfilt_Q_min_len", ["base"])
    shell:
        """
        fastq-filter --min-length {wildcards.L}000 --mean-quality {wildcards.Q} -o {output} {wildcards.base}.fastq.gz
        """

rule fastq_filt_Q_max_len:
    input:
        "{base}.fastq.gz"
    output:
        "{base}.Q{Q}.max_{L}k.fastq.gz"
    conda:
        "envs/mini.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("fastqfilt_Q_max_len", ["base"])
    shell:
        """
        fastq-filter --max-length {wildcards.L}000 --mean-quality {wildcards.Q} -o {output} {wildcards.base}.fastq.gz
        """

#ONTA1B1C1
rule fastq_to_q20:
    input:
        "{base}.fastq.gz"
    output:
        "{base}.Q20.fastq.gz"
    conda:
        "envs/mini.yml"
    threads:
        config['cpu_aux']
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("fastq2q20", ["base"])
    shell:
        """
        fastq-filter -q 20 -o {output} {wildcards.base}.fastq.gz
        """

if("sr1" in config and "sr2" in config and "basename" in config):
    rule genomescope:
        input:
            r1=config['sr1'],
            r2=config["sr2"]
        output:
            tar=config["basename"] + ".gscope2.tar.gz",
            summary=config['basename'] + ".gscope2.summary.txt"
        params:
            kmer = 21,
            fastqs = [config['sr1'][:-3], config['sr2'][:-3]],
            opt_gscope = opts["genomescope2"],
            base = config["basename"] + ".gscope2"
        conda:
            "envs/readqc.yml"
        group:
            gen_group("gscop2_" + basesuffix, [])
        resources:
            mem_mb=config["mem_aux"]
        threads:
            config["cpu_aux"]
        shell:
            """
            gunzip {input.r1}
            gunzip {input.r2}
            jellyfish count -C -m {params.kmer} -s 1000000000 -t {threads} {params.fastqs} -o reads.jf
            jellyfish histo -t {threads} reads.jf > reads.histo
            mkdir -p $(dirname {params.base})
            genomescope2 -i reads.histo -o {params.base} -k {params.kmer} {params.opt_gscope}
            mv reads.histo {params.base}
            tar -zcf {output.tar} {params.base}
            mv {params.base}/summary.txt {output.summary}
            """

if("hifi" in config and "basename" in config):
    rule hifi_genomescope:
        input:
            reads=config['hifi']
        output:
            tar=config["basename"] + ".hifi_gscope2.tar.gz",
            summary=config['basename'] + ".hifi_gscope2.summary.txt"
        params:
            kmer = 21,
            opt_gscope = opts["genomescope2"],
            base = config["basename"] + ".hifi_gscope2",
            basename = config["basename"]
        conda:
            "envs/readqc.yml"
        group:
            gen_group("hifi_gscop2_" + basesuffix, [])
        resources:
            mem_mb=config["mem_aux"]
        threads:
            config["cpu_aux"]
        shell:
            """
            meryl count threads={threads} k={params.kmer} output {params.basename}.hifi.meryl {input.reads}
            meryl histogram {params.basename}.hifi.meryl > reads.histo
            sed -i 's/\\t/ /g' reads.histo
            mkdir -p $(dirname {params.base})
            genomescope2 -i reads.histo -o {params.base} -k {params.kmer} {params.opt_gscope}
            mv reads.histo {params.base}
            tar -zcf {output.tar} {params.base}
            mv {params.base}/summary.txt {output.summary}
            """

if("basename" in config and "ont" in config):
    rule miniasm:
        input:
            config["ont"]
        output:
            gfa=f"{config['basename']}.miniasm.gfa",
            fasta=f"{config['basename']}.miniasm.fasta"
        params:
            opt_overlap = opts["overlap"],
            opt_miniasm = opts["miniasm"] 
        threads:
            config['cpu_assembly']
        conda:
            "envs/mini.yml"
        resources:
            mem_mb=config['mem_assembly']
        group:
            gen_group("miniasm_" + basesuffix, [])
        shell:
            """
            mkdir -p $(dirname {output.fasta})
            minimap2 {params.opt_overlap} -x ava-ont -t {threads} {input} {input} > overlaps.paf
            miniasm {params.opt_miniasm} -f {input} overlaps.paf > {output.gfa}
            awk \'/^S/{{print ">"$2"\\n"$3}}\' {output.gfa} | fold > {output.fasta}
            """


if("basename" in config and ("ont" in config or "hifi" in config)):
    rule flye:
        input:
            lambda wcs: config['ont'] if wcs['suf'] == "HQ" else config['hifi']
        output:
            fasta=f"{config['basename']}.flye_{{suf}}.fasta",
            gfa=f"{config['basename']}.flye_{{suf}}.gfa"
        params:
            outdir = config['basename'] + ".flye",
            opt = opts["flye"],
            input_opt = lambda wcs: {"HIFI": "--pacbio-hifi", "HQ": "--nano-hq"}[wcs['suf']],
            size = f"--genome-size {config['genome_size']}" if("genome_size" in config) else ""
        threads:
            config['cpu_assembly']
        conda:
            "envs/flye.yml"
        wildcard_constraints:
            suf="HQ|HIFI"
        resources:
            mem_mb=config['mem_assembly']
        group:
            gen_group("flye_" + basesuffix, ["suf"])
        shell:
            """
            mkdir -p {params.outdir}
            flye {params.opt} {params.size} --threads {threads} \\
                {params.input_opt} {input} \\
                --out-dir {params.outdir}
            mv {params.outdir}/assembly.fasta {output.fasta}
            mv {params.outdir}/assembly_graph.gfa {output.gfa}
            """



if("basename" in config and "ont" in config and "genome_size" in config):
    rule wtdbg2:
        input:
            lambda wcs: config["ont"]
        output:
            f"{config['basename']}.wtdbg2.fasta"
        params:
            outdir = config['basename'] + ".wtdbg2",
            size = lambda wcs: config["genome_size"],
            opt_wtdbg2 = opts["wtdbg2"],
            opt_cns = opts["wtpoa-cns"]
        threads:
            config['cpu_assembly']
        conda:
            "envs/wtdbg.yml"
        resources:
            mem_mb=config['mem_assembly']
        group:
            gen_group("wtdbg2_" + basesuffix, [])
        shell:
            """
            mkdir -p {params.outdir}
            wtdbg2 {params.opt_wtdbg2} -t {threads} -x ont -g {params.size} \\
                -i {input} \\
                -o {params.outdir}/assembly
            wtpoa-cns {params.opt_cns} -t {threads} \\
                -i {params.outdir}/assembly.ctg.lay.gz \\
                -fo {params.outdir}/assembly.ctg.slf.fa
            mv {params.outdir}/assembly.ctg.slf.fa {output}
            """


def spades_input(wcs):
    ret = {
        "r1": config["sr1"],
        "r2": config["sr2"]
    }
    if("reads" in config):
        ret["long"] = config["ont"]
    return ret


if('basename' in config and "sr1" in config and "sr2" in config):
    rule spades:
        input:
            unpack(spades_input)
        output:
            f"{config['basename']}.spades.fasta"
        params:
            lr = "" if "reads" not in config else f"--nanopore {config['reads']}",
            opt = opts["spades"],
            outdir = f"{config['basename']}.spades",
            mem_gb = int(config['mem_assembly'] / 1000)
        conda:
            "envs/spades.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("spades_" + basesuffix, [])
        shell:
            """
            mkdir -p {params.outdir}
            spades.py {params.opt} -t {threads} -m {params.mem_gb} {params.lr} \\
                -o {params.outdir} \\
                -1 {input.r1} \\
                -2 {input.r2}
            mv {params.outdir}/scaffolds.fasta {output}
            """


if("hifi" in config and "basename" in config):
    rule hifiasm:
        input:
            reads=config["hifi"]
        output:
            tar=f"{config['basename']}.hifiasm.tar.gz",
            fasta=f"{config['basename']}.hifiasm.fasta",
            hap1=f"{config['basename']}.hifiasm.hap1.fasta",
            hap2=f"{config['basename']}.hifiasm.hap2.fasta"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm"
        conda:
            "envs/HIFI.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_" + basesuffix, [])
        shell:
            """
            BD=$(basename {params.outbase})
            mkdir $BD
            hifiasm -o $BD/$BD -t {threads} {params.opt} {input.reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.bp.p_ctg.gfa > {output.fasta}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.bp.hap1.p_ctg.gfa > {output.hap1}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.bp.hap2.p_ctg.gfa > {output.hap2}
            """

if("hifi" in config and "ont" in config and "basename" in config):
    rule hifiasm_ulr:
        input:
            hifi_reads=config["hifi"],
            ont_reads=config["ont"]
        output:
            tar=f"{config['basename']}.hifiasm_ulr.tar.gz",
            fasta=f"{config['basename']}.hifiasm_ulr.fasta",
            hap1=f"{config['basename']}.hifiasm_ulr.hap1.fasta",
            hap2=f"{config['basename']}.hifiasm_ulr.hap2.fasta"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm_ulr"
        conda:
            "envs/HIFI.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_ulr_" + basesuffix, [])
        shell:
            """
            BD=$(basename {params.outbase})
            mkdir $BD
            hifiasm -o $BD/$BD -t {threads} {params.opt} --ul {input.ont_reads} {input.hifi_reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.bp.p_ctg.gfa > {output.fasta}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.bp.hap1.p_ctg.gfa > {output.hap1}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.bp.hap2.p_ctg.gfa > {output.hap2}
            """

if("hifi" in config and "ont" in config and "basename" in config and "hic1" in config and "hic2" in config):
    rule hifiasm_ulr_hic:
        input:
            hifi_reads=config["hifi"],
            ont_reads=config["ont"],
            hic1=config['hic1'],
            hic2=config['hic2']
        output:
            tar=f"{config['basename']}.hifiasm_ulr_hic.tar.gz",
            fasta=f"{config['basename']}.hifiasm_ulr_hic.fasta",
            hap1=f"{config['basename']}.hifiasm_ulr_hic.hap1.fasta",
            hap2=f"{config['basename']}.hifiasm_ulr_hic.hap2.fasta"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm_ulr_hic"
        conda:
            "envs/HIFI.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_ulr_hic_" + basesuffix, [])
        shell:
            """
            BD=$(basename {params.outbase})
            mkdir $BD
            hifiasm -o $BD/$BD -t {threads} {params.opt} --ul {input.ont_reads} --h1 {input.hic1} --h2 {input.hic2} {input.hifi_reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.hic.p_ctg.gfa > {output.fasta}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.hic.hap1.p_ctg.gfa > {output.hap1}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.hic.hap2.p_ctg.gfa > {output.hap2}
            """

if("hifi" in config and "basename" in config and "hic1" in config and "hic2" in config):
    rule hifiasm_hic:
        input:
            reads=config["hifi"],
            hic1=config['hic1'],
            hic2=config['hic2']
        output:
            tar=f"{config['basename']}.hifiasm_hic.tar.gz",
            fasta=f"{config['basename']}.hifiasm_hic.fasta",
            hap1=f"{config['basename']}.hifiasm_hic.hap1.fasta",
            hap2=f"{config['basename']}.hifiasm_hic.hap2.fasta"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm_hic"
        conda:
            "envs/HIFI.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_hic_" + basesuffix, [])
        shell:
            """
            BD=$(basename {params.outbase})
            mkdir $BD
            hifiasm -o $BD/$BD -t {threads} {params.opt} --h1 {input.hic1} --h2 {input.hic2} {input.reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.hic.p_ctg.gfa > {output.fasta}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.hic.hap1.p_ctg.gfa > {output.hap1}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.hic.hap2.p_ctg.gfa > {output.hap2}
            """



if("hifi" in config and "basename" in config and "par1" in config and "par2" in config):
    rule hifiasm_trio:
        input:
            reads=config["hifi"],
            par1=config['par1'],
            par2=config['par2']
        output:
            tar=f"{config['basename']}.hifiasm_trio.tar.gz",
            hap1=f"{config['basename']}.hifiasm_trio.hap1.fasta",
            hap2=f"{config['basename']}.hifiasm_trio.hap2.fasta",
            trioeval1=f"{config['basename']}.hifiasm_trio.hap1.trioeval.txt",
            trioeval2=f"{config['basename']}.hifiasm_trio.hap2.trioeval.txt"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm_trio"
        conda:
            "envs/HIFI.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_trio_" + basesuffix, [])
        shell:
            """
            BD=$(basename {params.outbase})
            mkdir $BD
            yak count -k31 -b37 -t{threads} -o par1.yak {input.par1}
            yak count -k31 -b37 -t{threads} -o par2.yak {input.par2}
            hifiasm -o $BD/$BD -t {threads} {params.opt} -1 par1.yak -2 par2.yak {input.reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.dip.hap1.p_ctg.gfa > {output.hap1}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.dip.hap2.p_ctg.gfa > {output.hap2}
            yak trioeval par1.yak par2.yak {output.hap1} > {output.trioeval1}
            yak trioeval par1.yak par2.yak {output.hap2} > {output.trioeval2}
            """

if("hifi" in config and "basename" in config and "par1" in config and "par2" in config and "ont" in config):
    rule hifiasm_ulr_trio:
        input:
            reads=config["hifi"],
            par1=config['par1'],
            par2=config['par2'],
            ont_reads=config["ont"],
        output:
            tar=f"{config['basename']}.hifiasm_ulr_trio.tar.gz",
            hap1=f"{config['basename']}.hifiasm_ulr_trio.hap1.fasta",
            hap2=f"{config['basename']}.hifiasm_ulr_trio.hap2.fasta",
            trioeval1=f"{config['basename']}.hifiasm_ulr_trio.hap1.trioeval.txt",
            trioeval2=f"{config['basename']}.hifiasm_ulr_trio.hap2.trioeval.txt"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm_ulr_trio"
        conda:
            "envs/HIFI.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_ulr_trio_" + basesuffix, [])
        shell:
            """
            BD=$(basename {params.outbase})
            mkdir $BD
            yak count -k31 -b37 -t{threads} -o par1.yak {input.par1}
            yak count -k31 -b37 -t{threads} -o par2.yak {input.par2}
            hifiasm -o $BD/$BD -t {threads} {params.opt} --ul {input.ont_reads} -1 par1.yak -2 par2.yak {input.reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.dip.hap1.p_ctg.gfa > {output.hap1}
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.dip.hap2.p_ctg.gfa > {output.hap2}
            yak trioeval par1.yak par2.yak {output.hap1} > {output.trioeval1}
            yak trioeval par1.yak par2.yak {output.hap2} > {output.trioeval2}
            """

if("hifi" in config and "basename" in config):
    rule hifiasm_meta:
        input:
            reads=config["hifi"],
        output:
            tar=f"{config['basename']}.hifiasm_meta.tar.gz",
            fasta=f"{config['basename']}.hifiasm_meta.fasta"
        params:
            opt = opts['hifiasm'],
            outbase = config['basename'] + ".hifiasm_meta"
        conda:
            "envs/HIFI_meta.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("hifiasm_meta_" + basesuffix, [])
        shell:
            """
            wget https://zlib.net/zlib-1.2.13.tar.gz
            tar -xvf zlib-1.2.13.tar.gz
            cd zlib-1.2.13
            ./configure
            make
            export PATH="/usr/local/include:$PATH"
            export PATH="/usr/local/lib:$PATH"
            export LD_LIBRARY_PATH=/usr/local/lib
            export C_INCLUDE_PATH=/usr/local/include
            export CPLUS_INCLUDE_PATH=/usr/local/include
            cd ..
            wget https://github.com/xfengnefx/hifiasm-meta/archive/refs/tags/hamtv0.3.1.tar.gz
            tar -xvf hamtv0.3.1.tar.gz
            cd hifiasm-meta-hamtv0.3.1/ && make
            cd ..
            BD=$(basename {params.outbase})
            mkdir $BD
            ./hifiasm-meta-hamtv0.3.1/hifiasm_meta -o $BD/$BD -t {threads} {params.opt} {input.reads}
            rm $BD/*.bin
            mkdir -p $(dirname {params.outbase})
            tar -zcf {params.outbase}.tar.gz $BD/
            awk '/^S/{{print ">"$2;print $3}}' $BD/$BD.p_ctg.gfa > {output.fasta}
            """

if("hifi" in config and "basename" in config):
    rule IPA:
        input:
            reads=config["hifi"]
        output:
            tar=f"{config['basename']}.ipa.tar.gz",
            prim=f"{config['basename']}.ipa_p_ctg.fasta",
            alt=f"{config['basename']}.ipa_a_ctg.fasta"
        params:
            opt = opts['ipa']
        conda:
            "envs/ipa.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("IPA_" + basesuffix, [])
        shell:
            """
            ln -s $(which time) /usr/bin/time
            ipa local {params.opt} --input-fn {input}  --nthreads {threads} --njobs 1
            mkdir -p $(dirname {output.prim})
            cp RUN/19-final/final.p_ctg.fasta {output.prim}
            cp RUN/19-final/final.a_ctg.fasta {output.alt}
            tar -zcf {output.tar} RUN/
            """


if("hifi" in config and "ont" in config and "basename" in config):
    rule verkko:
        input:
            ont = config['ont'],
            hifi = config['hifi']
        output:
            tar = f"{config['basename']}.verkko.tar.gz",
            fasta = f"{config['basename']}.verkko.fasta"
        params:
            opt = opts['verkko'],
            outbase = config['basename'] + ".verkko",
            mem_gb = int(config['mem_assembly'] / 1000)
        conda:
            "envs/verkko.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("verkko_" + basesuffix, [])
        shell:
            """
            sed -i 's|#!/bin/sh|#!/bin/bash|' /data1/snakemake/.snakemake/conda/*/bin/verkko
            mkdir -p $(dirname {params.outbase})
            verkko --local --local-memory {params.mem_gb} --local-cpus {threads} {params.opt} \\
                -d {params.outbase} \\
                --hifi {input.hifi} \\
                --nano {input.ont}
            tar -zcf {output.tar} {params.outbase}/
            cp {params.outbase}/assembly.fasta {output.fasta}
            """

if("hifi" in config and "ont" in config and "basename" in config and "hic1" in config and "hic2" in config):
    rule verkko_hic:
        input:
            ont = config['ont'],
            hifi = config['hifi'],
            hic1 = config['hic1'],
            hic2 = config['hic2']
        output:
            tar = f"{config['basename']}.verkko_hic.tar.gz",
            fasta = f"{config['basename']}.verkko_hic.fasta",
            hap1 = f"{config['basename']}.verkko_hic.hap1.fasta",
            hap2 = f"{config['basename']}.verkko_hic.hap2.fasta"
        params:
            opt = opts['verkko'],
            outbase = config['basename'] + ".verkko_hic",
            mem_gb = int(config['mem_assembly'] / 1000)
        conda:
            "envs/verkko.yml"
        resources:
            mem_mb=config['mem_assembly']
        threads:
            config['cpu_assembly']
        group:
            gen_group("verkko_hic_" + basesuffix, [])
        shell:
            """
            sed -i 's|#!/bin/sh|#!/bin/bash|' /data1/snakemake/.snakemake/conda/*/bin/verkko
            mkdir -p $(dirname {params.outbase})
            verkko --local --local-memory {params.mem_gb} --local-cpus {threads} {params.opt} \\
                -d {params.outbase} \\
                --hifi {input.hifi} \\
                --nano {input.ont} \\
                --hic1 {input.hic1} \\
                --hic2 {input.hic2}
            tar -zcf {output.tar} {params.outbase}/
            cp {params.outbase}/assembly.fasta {output.fasta}
            cp {params.outbase}/assembly.haplotype1.fasta {output.hap1}
            cp {params.outbase}/assembly.haplotype2.fasta {output.hap2}
            """

if("ont" in config):
    rule racon:
        input:
            "{base}.fasta",
            config["ont"]
        output:
            "{base}.racon{n}.fasta"
        params:
            opt_minimap2 = opts["minimap2_racon"],
            opt_racon = opts["racon"]
        threads:
            config['cpu_polish']
        wildcard_constraints:
            n = "\d"
        conda:
            "envs/mini.yml"
        resources:
            mem_mb=config['mem_polish']
        group:
            gen_group("racon", ["base"])
        shell:
            """
            CURRENT={input[0]}
            for i in {{1..{wildcards.n}}}
            do
                minimap2 {params.opt_minimap2} -x map-ont -t {threads} $CURRENT \\
                    {input[1]} \\
                    > {wildcards.base}.mm2.paf
                racon {params.opt_racon} -t {threads} \\
                    {input[1]} \\
                    {wildcards.base}.mm2.paf \\
                    $CURRENT > {wildcards.base}.racon$i.fasta
                CURRENT={wildcards.base}.racon$i.fasta
            done
            """


if("sr1" in config and "sr2" in config):
    rule pilon:
        input:
            "{base}.fasta",
            config["sr1"],
            config["sr2"]
        output:
            "{base}.pilon{n}.fasta"
        params:
            outbase = lambda wcs : f"{wcs['base']}.pilon{wcs['n']}",
            mem = int(config['mem_polish'] * 0.9),
            opt_minimap2 = opts["minimap2_pilon"],
            opt_pilon = opts["pilon"]
        threads:
            config['cpu_polish']
        wildcard_constraints:
            n = "\d"
        conda:
            "envs/mini.yml"
        resources:
            mem_mb=config['mem_polish']
        group:
            gen_group("pilon", ["base"])
        shell:
            """
            CURRENT={input[0]}
            for i in {{1..{wildcards.n}}}
            do
                minimap2 {params.opt_minimap2} -ax sr -t {threads} $CURRENT \\
                    {input[1]} \\
                    {input[2]} \\
                    | samtools sort \\
                    > {wildcards.base}.mm2.sr.bam
                samtools index {wildcards.base}.mm2.sr.bam
                pilon {params.opt_pilon} -Xmx{params.mem}M --threads {threads} \\
                    --genome $CURRENT \\
                    --bam {wildcards.base}.mm2.sr.bam \\
                    --output {wildcards.base}.pilon$i
                CURRENT={wildcards.base}.pilon$i.fasta
            done
            """


rule ph_gen_hist:
    input:
        ref="{base}.fasta",
        reads=lambda wcs: [config["ont"]] if(wcs["rt"] == "lr") else [config["sr1"], config["sr2"]]
    output:
        "{base}.ph_{rt}.bam.gencov",
        "{base}.ph_{rt}.bam.stats",
        "{base}.ph_{rt}.bam"
    params:
        outbase=lambda wcs: "{base}.ph_{rt}".format(**wcs),
        map_mode=lambda wcs: "map-ont" if(wcs['rt'] == "lr") else "sr"
    conda:
            "envs/mini.yml"
    wildcard_constraints:
        rt = "sr|lr"
    resources:
        mem_mb=config["mem_polish"]
    threads:
        config["cpu_polish"]
    group:
        gen_group("ph_hist", ["base", "rt"])
    shell:
        """
        minimap2 -ax {params.map_mode} -t {threads} {input.ref} {input.reads} | samtools sort > {params.outbase}.bam
        samtools stats {params.outbase}.bam > {output[1]}
        purge_haplotigs hist -b {params.outbase}.bam -g {input.ref} || true
        """


rule ph_purge:
    input:
        "{base}.fasta",
        "{base}.ph_{rt}.bam.gencov"
    output:
        "{base}.ph_{rt}_l{low}_m{mid}_h{high}.fasta",
        "{base}.ph_{rt}_l{low}_m{mid}_h{high}.haplotigs.fasta"
    params:
        low=lambda wcs: wcs["low"],
        mid=lambda wcs: wcs["mid"],
        high=lambda wcs: wcs["high"],
        outbase=lambda wcs: "{base}.ph_{rt}_l{low}_m{mid}_h{high}".format(**wcs)
    conda:
        "envs/mini.yml"
    wildcard_constraints:
        rt = "sr|lr",
        low = "\d+",
        mid = "\d+",
        high = "\d+"
    resources:
        mem_mb=config["mem_polish"]
    threads:
        config["cpu_polish"]
    group:
        gen_group("ph_hist", ["base", "rt", "low", "mid", "high"])
    shell:
        """
        purge_haplotigs cov -i {input[1]} -l {params.low} -m {params.mid} -h {params.high} -o {params.outbase}.coverage_stats.csv
        samtools faidx {input[0]}
        purge_haplotigs purge -t {threads} -g {input[0]} -c {params.outbase}.coverage_stats.csv -o {params.outbase}
        """

rule fcs_gx:
    input:
        "{base}.fasta"
    output:
        clean_fasta = "{base}.fcs_gx_{taxid}.fasta",
        fcs_report = "{base}.fcs_gx/{taxid}.fcs_gx_report.txt",
        fcs_rpt = "{base}.fcs_gx/{taxid}.taxonomy.rpt",
        contam_fasta = "{base}.fcs_gx/{taxid}.contam.fasta",
        fcs_log = "{base}.fcs_gx/{taxid}.fcs_gx.log"
    params:
        taxid = lambda wcs: wcs["taxid"],
        outdir = "outdir",
        base = lambda wcs : wcs["base"]
    conda:
        "envs/fcs.yml"
    resources:
        mem_mb = 500000
    threads:
        32
    group:
        gen_group("fcs_gx", ["base","taxid"])
    wildcard_constraints:
        taxid = "\d+"
    shell:
        """
        BASENAME=$(basename {params.base})
        mkdir -p {params.base}.fcs_gx/
        mkdir tmpfs
        mount -t tmpfs tmpfs tmpfs -o size=470G
        s5cmd  --no-sign-request cp  --part-size 50  --concurrency 50 s3://ncbi-fcs-gx/gxdb/latest/all.* tmpfs/fcs_gx_db/
        curl https://ftp.ncbi.nlm.nih.gov/genomes/TOOLS/FCS/releases/latest/fcs-gx.sif -Lo fcs-gx.sif
        curl -LO https://github.com/ncbi/fcs/raw/main/dist/fcs.py
        python fcs.py --image fcs-gx.sif screen  genome \\
            --fasta {input} \\
            --out-dir {params.outdir} \\
            --gx-db tmpfs/fcs_gx_db/ \\
            --tax-id {params.taxid} \\
            2>&1 | tee fcs.log

        cat {input} | python fcs.py --image fcs-gx.sif clean genome \\
            --action-report {params.outdir}/$BASENAME.{params.taxid}.fcs_gx_report.txt \\
            --output {output.clean_fasta} \\
            --contam-fasta-out {output.contam_fasta} \\
            2>&1 | tee -a fcs.log
        mv fcs.log {output.fcs_log}
        mv {params.outdir}/$BASENAME.{params.taxid}.fcs_gx_report.txt {output.fcs_report}
        mv {params.outdir}/$BASENAME.{params.taxid}.taxonomy.rpt {output.fcs_rpt}
    
        """


rule busco:
    input:
        "{base}.fasta"
    output:
        "{base}.busco_summary_{busco_db}.txt",
        "{base}.busco_table_{busco_db}.tsv"
    params:
        opt = opts["busco"]
    wildcard_constraints:
        busco_db = "|".join(busco_datasets)
    threads:
        config['cpu_aux']
    conda:
        "envs/busco.yml"
    resources:
        mem_mb=config['mem_aux']
    group:
        gen_group("busco", ["base", "busco_db"])
    shell:
        """
        awk '{{ if(substr($1,1,1) == ">"){{print ">" ++count}} else {{print}} }}' {input} > {wildcards.base}.renamed.fasta
        BN=$(basename {wildcards.base})
        busco {params.opt} -i {wildcards.base}.renamed.fasta -c {threads} --mode geno -o $BN -l {wildcards.busco_db}
        mv $BN/run_{wildcards.busco_db}/short_summary.txt {output[0]}
        mv $BN/run_{wildcards.busco_db}/full_table.tsv {output[1]}
        """


rule helixer:
    input:
        "{base}.fasta"
    output:
        "{base}.helixer_{model}.gff3"
    wildcard_constraints:
        model = "land_plant|vertebrate|invertebrate|fungi"
    params:
        model = lambda wcs: wcs["model"],
        opt = opts["helixer"]
    container:
        "docker://gglyptodon/helixer-docker:helixer_v0.3.2_cuda_11.8.0-cudnn8"
    group:
        gen_group("annot", ["base"])
    resources:
        mem_mb=100000
    threads:
        32
    shell:
        """
        fetch_helixer_models.py -l {params.model}
        Helixer.py {params.opt} \\
            --fasta-path {input} \\
            --gff-output-path {output} \\
            --lineage {params.model} \\
            --species $(basename {wildcards.base})
        """


rule annotation_post:
    input:
        ref = "{base}.fasta",
        gff = "{base}.{annot}.gff3"
    output:
        cds = "{base}.{annot}.cds.fasta",
        prot = "{base}.{annot}.prot.fasta",
        mrna = "{base}.{annot}.mrna.fasta"
    wildcard_constraints:
        model = "land_plant|vertebrate|invertebrate|fungi",
        annot = "helixer_.*"
    threads:
        config['cpu_aux']
    conda:
        "envs/bio_utils.yml"
    resources:
        mem_mb=config['mem_aux']
    group:
        gen_group("buscoP", ["base"])
    shell:
        """
        gffread \\
            -w {output.mrna} \\
            -y {output.prot} \\
            -x {output.cds} \\
            -g {input.ref} \\
            {input.gff}
        """


rule busco_prot:
    input:
        ref = "{base}.prot.fasta"
    output:
        summary = "{base}.buscoP_summary_{busco_db}.txt",
        table = "{base}.buscoP_table_{busco_db}.tsv"
    wildcard_constraints:
        busco_db = "|".join(busco_datasets),
    threads:
        config['cpu_aux']
    conda:
        "envs/busco.yml"
    resources:
        mem_mb=config['mem_aux']
    group:
        gen_group("busco", ["base", "busco_db"])
    shell:
        """
        BN=$(basename {wildcards.base})
        busco -c {threads} --mode prot \\
            -i {input.ref} \\
            -o $BN \\
            -l {wildcards.busco_db}
        mv $BN/run_{wildcards.busco_db}/short_summary.txt {output.summary}
        mv $BN/run_{wildcards.busco_db}/full_table.tsv {output.table}
        """


rule assembly_stats:
    input:
        "{base}.fasta"
    output:
        "{base}.assembly_stats.tsv"
    conda:
        "envs/readqc.yml"
    threads:
        config["cpu_aux"]
    resources:
        mem_mb=config["mem_aux"]
    group:
        gen_group("stats", ["base"])
    shell:
        """
        assembly-stats -t {input} > {output}
        """


if("sr1" in config and "sr2" in config and "basename" in config):
    rule meryl:
        input:
            r1=config["sr1"],
            r2=config["sr2"]
        output:
            f"{config['basename']}.meryl.tar.gz"
        params:
            basename = config['basename'],
            ksize = 21
        conda:
            "envs/readqc.yml"
        threads:
            config["cpu_aux"]
        resources:
            mem_mb=config["mem_aux"]
        group:
            gen_group("meryl_" + basesuffix, [])
        shell:
            """
            mkdir -p $(dirname {output})
            meryl k={params.ksize} count output {params.basename}_1.meryl {input.r1}
            meryl k={params.ksize} count output {params.basename}_2.meryl {input.r2}
            meryl union-sum output {params.basename}.meryl {params.basename}_1.meryl {params.basename}_2.meryl
            tar -zcf {params.basename}.meryl.tar.gz {params.basename}.meryl
            """


if("sr1" in config and "sr2" in config and "basename" in config):
    rule merqury:
        input:
            fasta="{base}.fasta",
            kmers=f"{config['basename']}.meryl.tar.gz"
        output:
            qv="{base}.merqury.qv"
        params:
            basename = config['basename']
        conda:
            "envs/readqc.yml"
        threads:
            config["cpu_aux"]
        resources:
            mem_mb=config["mem_aux"]
        group:
            gen_group("merqury", ["base"])
        shell:
            """
            tar -zxf {input.kmers}
            merqury.sh {params.basename}.meryl {input.fasta} merqury
            mv merqury.qv {output.qv}
            """


