#!/usr/bin/env python3

import mysql.connector
import boto3
import os
import json
import pandas
import sys
import argparse
from misc_structs import prim_key, database_keys


create_template = """CREATE TABLE {tbl}(
    {cols},
    PRIMARY KEY ({prim})
);"""

insert_template = """REPLACE INTO {tbl} (
    {cols}
) VALUES (
    {vals}
);"""

query_template = """SELECT * from {tbl};"""


def get_rds_credentials(rds_json):
    with open(rds_json) as fin:
        cred = json.load(fin)
    return cred


def execute_command(cred, cmd, dry=True, hasreturn=False):
    if(dry):
        print(cmd)
        return
    os.environ['LIBMYSQL_ENABLE_CLEARTEXT_PLUGIN'] = '1'
    try:
        session = boto3.Session(profile_name='default')
        client = session.client('rds')
        conn =  mysql.connector.connect(
            host=cred["ENDPOINT"],
            user=cred["USER"],
            passwd=cred["PASSWD"],
            port=cred["PORT"],
            database=cred["DBNAME"]
        )
        cur = conn.cursor()
        cur.execute(cmd)
        if(hasreturn):
            result = cur.fetchall()
        else:
            conn.commit()
            result = None
        cur.close()
        conn.close()
        return result
    except Exception as e:
        print("Database connection failed due to {}".format(e))  


def create_table(dryrun, rds_json):
    cred = get_rds_credentials(rds_json)
    create_cmd = create_template.format(
        tbl=cred['ASSEMBLY_TBL'],
        cols=",\n    ".join(" ".join(t) for t in database_keys),
        prim=prim_key
    )
    execute_command(cred, create_cmd, dryrun, False)


def sqlify_value(v):
    if v is None:
        return "NULL"
    elif(isinstance(v, str)):
        return f'"{v}"'
    else:
        return str(v)


def insert_data(data, rds_json):
    stringified = {k: sqlify_value(v) for k, v in data.items()}
    cred = get_rds_credentials(rds_json)
    insert_cmd = insert_template.format(
        tbl=cred['ASSEMBLY_TBL'],
        cols=", ".join(t[0] for t in database_keys),
        vals=", ".join(stringified.get(c[0], "NULL") for c in database_keys)
    )
    execute_command(cred, insert_cmd, False, False)


def fetch_table(rds_json):
    cred = get_rds_credentials(rds_json)
    query_cmd = query_template.format(tbl=cred['ASSEMBLY_TBL'])
    res = execute_command(cred, query_cmd, False, True)
    return pandas.DataFrame(res, columns=[t[0] for t in database_keys])


def cli_main():
    parser = argparse.ArgumentParser(description=(
        "Tools for interacting with assembly_stats tables. "
        "Configuration should be at ~/.aws/rds.json"
    ))
    parser.set_defaults(which="help")
    subparsers = parser.add_subparsers()
    sub = subparsers.add_parser("create", help="initialize the assembly_stats table based on relevant config.")
    sub.add_argument("-d", "--dryrun", action="store_true", help="print sql to screen but don't execute")
    sub.add_argument(
        "--rds_json",
        help="path to json file containing configuration for rds table",
        default=os.path.expanduser("~/.aws/rds.json")
    )
    sub.set_defaults(which="create")
    sub = subparsers.add_parser("query", help="query the assembly_stats table and write tsv to stdout")
    sub.add_argument(
        "--rds_json",
        help="path to json file containing configuration for rds table",
        default=os.path.expanduser("~/.aws/rds.json")
    )
    sub.set_defaults(which="query")
    args = parser.parse_args()
    if(args.which == 'query'):
        df = fetch_table(args.rds_json)
        df['Busco_C%'] = df.busco_C / df.busco_n
        df['Busco_D%'] = df.busco_D / df.busco_n
        df['ONT_Coverage'] = df.ont_nbases / df.assembly_total_len
        df['HiFi_Coverage'] = df.hifi_nbases / df.assembly_total_len
        df['Ill_Coverage'] = df.ill_nbases / df.assembly_total_len
        df.to_csv(sys.stdout, sep="\t", index=None)
    if(args.which == "create"):
        create_table(args.dryrun, args.rds_json)


if(__name__ == "__main__"):
    cli_main()