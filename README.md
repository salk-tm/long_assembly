# Overview
This snakemake workflow is designed to allow for the creation of various assemblies using ONT, HiFi, and/or Illumina data.

This pipeline is designed to use snakemake and tibanna to allow for assembling genomic ONT, HiFi, and Illumina reads using AWS. It supports [miniasm](https://github.com/lh3/miniasm), [flye](https://github.com/fenderglass/Flye), [Wtdbg2](https://github.com/ruanjue/wtdbg2), [hifiasm](https://github.com/chhylp123/hifiasm), [SPAdes](https://github.com/ablab/spades), [IPA](https://github.com/PacificBiosciences/pbipa), and [verkko](https://github.com/marbl/verkko) for assembling the reads. Depending on the data type and assembly method, it will also execute polishing as specified by the user using some combination of [racon](https://github.com/lbcb-sci/racon), and [pilon](https://github.com/broadinstitute/pilon). It will also run a variety of QC tools including [fcs-gx](https://github.com/ncbi/fcs-gx), [assembly-stats](https://anaconda.org/bioconda/assembly-stats), [BUSCO](https://gitlab.com/ezlab/busco), [nanoplot](https://github.com/wdecoster/NanoPlot), and [fastqc](https://github.com/s-andrews/FastQC). If using the primary entrypoint, after execution of the workflow, stats will be compiled and uploaded to a pre-configured rds table.

In order to run this pipeline using tibanna, you will need an aws account, at least one aws bucket, a working copy of snakemake, and will need to setup your tibanna unicorn. For details on how to setup tibanna, consult the [tibanna docs](https://tibanna.readthedocs.io/en/latest/). For full details on execution options, consult the [snakemake docs](https://snakemake.readthedocs.io/en/stable/)

# Primary Entrypoints

### ./tibanna_assembly.py
This is the primary entrypoint for assembly. The following options are required: s3prefix, outprefix, version, busco, and taxid. The "version" argument should be short (<4 characters). You will also need to provide input of some kind: --ont, --hifi, --hic1 and --hic2, --par1 and par2, or --sr1 and --sr2. You will also need to specify some assembly target: --miniasm, --flye_HQ, --flye_HIFI, --wtdbg2, --spades, --hifiasm, --hifiasm_hic, --hifiasm_trio, --ipa, and/or --verkko. Always remember to test your commands with "--dryrun" flag. If you are curious to see the generated snakemake command, you can use the "--print" option.

```
$ ./tibanna_assembly.py -h
usage: tibanna_assembly.py [-h] [--ont [ONT [ONT ...]]] [--hifi [HIFI [HIFI ...]]] [--sr1 SR1] [--sr2 SR2]
                           [--hic1 HIC1] [--hic2 HIC2] [--par1 [PAR1 [PAR1 ...]]] [--par2 [PAR2 [PAR2 ...]]]
                           [--genome_size GENOME_SIZE] [--ont_subset ONT_SUBSET] [--helixer_model HELIXER_MODEL]
                           [--miniasm] [--flye_HQ] [--flye_HIFI] [--wtdbg2] [--spades] [--hifiasm] [--hifiasm_hic]
                           [--hifiasm_trio] [--hifiasm_ulr_trio] [--hifiasm_meta] [--hifiasm_ulr] [--hifiasm_ulr_hic]
                           [--ipa] [--verkko] [--verkko_hic] [--profile {test,small,medium,large,extra,ultra}]
                           [--jobs JOBS] [--log_bucket LOG_BUCKET] [--print] [--dryrun] [--configfile CONFIGFILE
                           [--rds_json RDS_JSON] [--skipHelixer]
                           s3prefix outprefix version busco taxid

automates submitting an assembly job through tibanna for some dataset

optional arguments:
  -h, --help            show this help message and exit

Required:
  s3prefix              an s3 prefix that all files should share and be interpretted relative to
  outprefix             a prefix used for naming output files
  version               version identifier for assembly used for naming output files
  busco                 busco_db to use for evaluating assembly; Ex: liliopsida_odb10, eudicots_odb10
  taxid                 NCBI taxonomic ID for your sample

Input:
  --ont [ONT [ONT ...]]
                        s3 suffix path to ONT reads (default: [])
  --hifi [HIFI [HIFI ...]]
                        s3 suffix path to hifi reads (default: [])
  --sr1 SR1             s3 suffix path to R1 illumina reads (default: None)
  --sr2 SR2             s3 suffix path to R2 illumina reads (default: None)
  --hic1 HIC1           s3 suffix path to R1 hic reads (default: None)
  --hic2 HIC2           s3 suffix path to R2 hic reads (default: None)
  --par1 [PAR1 [PAR1 ...]]
                        s3 suffix path to parent1 hifi reads (default: [])
  --par2 [PAR2 [PAR2 ...]]
                        s3 suffix path to parent2 hifi reads (default: [])
  --genome_size GENOME_SIZE
                        a genome size estimate (default: None)
  --ont_subset ONT_SUBSET
                        if present, subset ont reads before assembly (default: None)
  --helixer_model HELIXER_MODEL
                        choice of gene prediction model. Defaults to land_plant (default: land_plant)

Assemblers:
  --miniasm             use ont reads to run miniasm, polish with racon, polish with pilon if sr1 and sr2 (default: False)
  --flye_HQ             use ont reads to run flye, polish with racon, then polish with pilon if sr1 and sr2 (default: False)
  --flye_HIFI           use hifi reads to run flye (default: False)
  --wtdbg2              use ont reads to run wtdbg2, polish with racon, then polish with pilon if sr1 and sr2 (default: False)
  --spades              use spades to assemble sr1 and sr2 (default: False)
  --hifiasm             use hifi reads to run hifiasm (default: False)
  --hifiasm_hic         use hifi reads and hic reads to run hifiasm (default: False)
  --hifiasm_trio        use hifi reads and parental reads to run hifiasm in trio mode (default: False)
  --hifiasm_ulr_trio    use hifi, ont, and parental reads to run hifiasm in trio mode with ultra-long input (default: False)
  --hifiasm_meta        use hifi reads to run hifiasm_meta (default: False)
  --hifiasm_ulr         use hifi and ont reads to run hifiasm with ultra-long input (default: False)
  --hifiasm_ulr_hic     use hifi, ont, and hic reads to run hifiasm with ultra-long input and hic haplotyping (default: False)
  --ipa                 use hifi reads to run IPA (default: False)
  --verkko              use hifi and ont reads to run Verkko (default: False)
  --verkko_hic          use hifi, ont, and hic reads to run verkko with ultra-long input and hic haplotyping (default: False)

Controls:
  --profile {test,small,medium,large,extra,ultra}
                        controls what size compute nodes will be used (default: large)
  --jobs JOBS, -j JOBS  controls how many compute nodes can be used (default: 1)
  --log_bucket LOG_BUCKET, -l LOG_BUCKET
                        log bucket for tibanna (default: salk-tm-logs)
  --print, -p           if present, only print the snakemake command and exit. (overrides dryrun) (default: False)
  --dryrun, -d          if present, do a snakemake dryrun and exit (default: False)
  --configfile CONFIGFILE
                        configfile to use for opt configuration (must be in configs/) (default: None)
  --rds_json RDS_JSON   path to json file containing configuration for rds table (default: /home/nallsing/.aws/rds.json)
  --skipHelixer         if present, does not perform helixer annotation (default: False)
```


### ./rds_tools.py
This script contains tools for initializing an rds database and for querying that database to generate a tsv report. Your mysql RDS instance should already be live and accessible with a database before attempting to run "./rds_tools.py create". Configurtation should be located in a json file at "~/.aws/rds.json". that json file should contain the following keys: ENDPOINT, PORT, REGION, USER, PASSWD, DBNAME, ASSEMBLY_TBL.

```
$ ./rds_tools.py -h
usage: rds_tools.py [-h] {create,query} ...

Tools for interacting with assembly_stats tables. Configuration should be at
~/.aws/rds.json

positional arguments:
  {create,query}
    create        initialize the assembly_stats table based on relevant
                  config.
    query         query the assembly_stats table and write tsv to stdout

optional arguments:
  -h, --help      show this help message and exit
```

# Example Usage - TM Lab Group

To run this pipeline, start by using ssh to login to our head node. Then activate the snake environment. then navigate to "~/workflows/long_assembly". Locate your reads that you want to assemble on aws (check salk-tm-raw/reads/?). For examples sake, lets say you want to assemble the ecoli data found at "salk-tm-dev/nolan_temp/ecoli_ass_test/". You might do something that looks like

```
$ aws s3 ls s3://salk-tm-dev/nolan_temp/ecoli_ass_test/ --human-readable | grep fastq.gz
2022-06-28 11:54:51   81.8 MiB SRR19754924_1.fastq.gz
2022-06-28 11:54:51   94.4 MiB SRR19754924_2.fastq.gz
2022-06-28 11:54:51  118.2 MiB hifi.fastq.gz
2022-06-28 11:54:51  244.1 MiB ont.fastq.gz

./tibanna_assembly.py \
    salk-tm-dev/nolan_temp/ecoli_ass_test/ \
    assemblies/ecoli \
    v3 \
    enterobacterales_odb10 \
    562 \
    --hifi hifi.fastq.gz \
    --sr1 SRR19754924_1.fastq.gz \
    --sr2 SRR19754924_2.fastq.gz \
    --ont ont.fastq.gz \
    --hifiasm \
    --dryrun
```

...This will execute a dry run. To actually kick off assembly. You might execute a command like...

```
nohup ./tibanna_assembly.py \
    salk-tm-dev/nolan_temp/ecoli_ass_test/ \
    assemblies/ecoli \
    v3 \
    enterobacterales_odb10 \
    562 \
    --hifi hifi.fastq.gz \
    --sr1 SRR19754924_1.fastq.gz \
    --sr2 SRR19754924_2.fastq.gz \
    --ont ont.fastq.gz \
    --hifiasm \
    > logs/ecoli.test.v3.txt 2>&1 &
```

# Manual workflow execution
If needed, you can directly invoke snakemake. All execution options are controlled via the snakemake config system and by declaring your own pipeline targets. For full details on using snakemake, consulte the [snakemake docs](https://snakemake.readthedocs.io/en/stable/).

Basically, every time you use this workflow, you will start by determining what file you are trying to create. You will then need to offer information to the workflow through the config in order to create those files. In order to use this workflow, you will need to learn what rules this workflow makes available to you, what the requirements for each are, and what the naming patterns are

## Rules

### fcs_gx
This rule will use [NCBI's Foreign Contamination Screening - GX Program](https://github.com/ncbi/fcs-gx) to filter out contaminated contigs after assembly. This rule requires the NCBI Taxonomy ID of the species being assembled.

1. input pattern: {base}.fasta
2. output pattern 1: {base}.fcs_gx_{taxid}.fasta
2. output pattern 1: {base}.fcs_gx/{taxid}.fcs_gx_report.txt
2. output pattern 1: {base}.fcs_gx/{taxid}.taxonomy.rpt
2. output pattern 1: {base}.fcs_gx/{taxid}.contam.fasta
2. output pattern 1: {base}.fcs_gx/{taxid}.fcs_gx.log

### subset_fastq
This rule allows you to subsample a fastq file down to a specific number of bases prior to assembly, keeping longest reads first. Where L should be something like "10G" to mean 10 gigabases. 

1. input pattern: {base}.fastq.gz
2. output pattern: {base}.sub_{L}.fastq.gz

### fastqc
This rule allows for running fastqc on some fastq data

1. input pattern: {prefix}.fastq.gz
2. output pattern: {prefix}_fastqc.html

### nanoplot
This rule allows for running nanoplot on some fastq data

1. input pattern: {prefix}.fastq.gz
2. output pattern: {prefix}_nanoplot.html

### hifi_bam_to_fastq
This rule allows for converting reads stored in bam format to reads in fastq.gz format. We are currently uploading hifi reads as bam files, so this rule exists to plug those bam files into the assemblers.

1. input pattern: {base}.bam
2. output pattern: {base}.hifi.fastq.gz

### fastq_to_q20
This rule allows for filtering of fastq reads to >= Q20. The filtered reads can then be used as HiFi reads.

1. input pattern: {base}.fastq.gz
2. output pattern: {base}.Q20.fastq.gz

### genomescope
This rule allows for running genomescope on your short reads.

1. Required config fields: basename, sr1, sr2
2. output pattern 1: {config.basename}.gscope2.tar.gz
3. output pattern 2: {config.basename}.gscope2.summary.txt

### miniasm
This rule will allow you to assemble ONT reads using the miniasm assembler.

1. Required config fields : basename, ont
2. output pattern 1 : {basename}.miniasm.fasta
3. output pattern 2 : {basename}.miniasm.gfa

### flye_HQ
This rule will allow you to assemble ONT reads using the flye assembler.

1. Required config fields : basename, ont
2. output pattern 1 : {basename}.flye_HQ.fasta
3. output pattern 2 : {basename}.flye_HQ.gfa

### flye_HIFI
This rule will allow you to assemble pacbio HIFI reads using the flye assembler.

1. Required config fields : basename, hifi
2. output pattern 1 : {basename}.flye_HIFI.fasta
3. output pattern 2 : {basename}.flye_HIFI.gfa

### wtdbg2
This rule will allow you to assemble ONT reads using the flye assembler.

1. Required config fields : basename, ont, genome_size
2. output pattern : {basename}.wtdbg2.fasta

### spades
This rule will allow you to assemble illumina reads using the spades assembler.

1. Required config fields : basename, sr1, sr2
2. output pattern : {basename}.spades.fasta

### hifiasm
This rule will allow you to assemble pacbio hifi reads using the hifiasm assembler

1. Required config fields : basename, hifi
2. output pattern 1 : {basename}.hifiasm.fasta
3. output pattern 2 : {basename}.hifiasm.hap1.fasta
4. output pattern 3 : {basename}.hifiasm.hap2.fasta
5. output pattern 4 : {basename}.hifiasm.tar.gz

### hifiasm_hic
This rule will allow you to assemble pacbio hifi and illumina HiC reads using the hifiasm assembler

1. Required config fields : basename, hifi, hic1, hic2
2. output pattern 1 : {basename}.hifiasm_hic.fasta
3. output pattern 2 : {basename}.hifiasm_hic.hap1.fasta
4. output pattern 3 : {basename}.hifiasm_hic.hap2.fasta
5. output pattern 4 : {basename}.hifiasm_hic.tar.gz

### hifiasm_trio
This rule will allow you to assemble CCS reads from a trio using the hifiasm assembler and outputs yak's trioeval stats

1. Required config fields : basename, hifi, par1, par2
2. output pattern 1 : {basename}.hifiasm_trio.hap1.fasta
3. output pattern 2 : {basename}.hifiasm_trio.hap2.fasta
4. output pattern 3 : {basename}.hifiasm_trio.tar.gz
5. output pattern 4 : {basename}.hifiasm_trio.hap1.trioeval.txt
6. output pattern 5 : {basename}.hifiasm_trio.hap2.trioeval.txt

### hifiasm_ulr
This rule will allow you to assemble both HiFi reads and ONT reads using the hifiasm assembler with the --ul flag

1. Required config fields : basename, hifi, ont
2. output pattern 1 : {basename}.hifiasm_ulr.hap1.fasta
3. output pattern 2 : {basename}.hifiasm_ulr.hap2.fasta
4. output pattern 3 : {basename}.hifiasm_ulr.fasta
5. output pattern 4 : {basename}.hifiasm_ulr.tar.gz

### hifiasm_ulr_hic
This rule will allow you to assemble HiFi, ONT, and HiC reads using the hifiasm assembler with the --ul and --h1/2 flags

1. Required config fields : basename, hifi, ont, hic1, hic2
2. output pattern 1 : {basename}.hifiasm_ulr_hic.hap1.fasta
3. output pattern 2 : {basename}.hifiasm_ulr_hic.hap2.fasta
4. output pattern 3 : {basename}.hifiasm_ulr_hic.fasta
5. output pattern 4 : {basename}.hifiasm_ulr_hic.tar.gz

### ipa
This rule will allow you to assemblye pacbio hifi reads using the ipa assembler

1. Required config fields : basename, hifi
2. output pattern 1 : {basename}.ipa_p_ctg.fasta
3. output pattern 2 : {basename}.ipa_a_ctg.fasta
4. output pattern 3 : {basename}.ipa.tar.gz

### verkko
This rule will allow you to assemble pacbio hifi and ont reads using the verkko assembler

1. Required config fields : basename, hifi, ont
2. output pattern 1 : {basename}.verkko.fasta
3. output pattern 2 : {basename}.verkko.tar.gz

### verkko_hic
This rule will allow you to assemble HiFi, ONT, and HiC reads using the verkko assembler with the --nano and --hic1/2 flags

1. Required config fields : basename, hifi, ont, hic1, hic2
2. output pattern 1 : {basename}.verkko_hic.hap1.fasta
3. output pattern 2 : {basename}.verkko_hic.hap2.fasta
4. output pattern 3 : {basename}.verkko_hic.fasta
5. output pattern 4 : {basename}.verkko_hic.tar.gz

### racon
This rule will allow you to polish an assembly using ONT reads and the racon consensuser. (k determines number of rounds of polishing)

1. Required config fields: ont
2. input pattern : {someprefix}.fasta
3. output pattern : {someprefix}.racon{k}.fasta

### pilon
This rule will allow you to polish an assembly using illumina reads and the pilon consensuser. (k determines the number of rounds of polishing)

1. Required config fields: sr1, sr2
2. input pattern : {someprefix}.fasta
3. output pattern : {someprefix}.pilon{k}.fasta

### busco
This rule will allow you to run busco on your assembly as a form of QC for your assembly and polishing. The busco database used is determined by the output file pattern requested by the user.

1. input pattern : {someprefix}.fasta
2. output pattern 1 : {someprefix}.busco_summary_{busco_db}.txt
3. output pattern 2 : {someprefix}.busco_table_{busco_db}.tsv


## Config Fields
The following keys are used to specify input and output for rules

### basename
The basename for the output assembly if assembly is requested. I'd reccomend using something short and descriptive of your sample. This is required for running any assembly operation or gscope.

### ont
the ONT reads that you wish to assemble and use for long read based polishers. These may be gzipped or not as the user likes. This is required for running flye_HQ, miniasm, wtdbg2, verkko, and racon

### hifi
the pacbio hifi reads that you wish to assemble. This is required for running flye_HIFI, hifiasm, IPA, and verkko.

### sr1
the "R1" illumina paired end reads that you want to use for pilon polishing or spades assembly. This read file must be paired with the file specfied by the sr2 key and this key is required if pilon, racon_short, or spades is requested by the user

### sr2
the "R2" illumina paired end reads that you want to use for pilon polishing or spades assembly. This read file must be paired with the file specfied by the sr1 key and this key is required if pilon, racon_short, or spades is requested by the user

### hic1
the "R1" illumina paired end HiC reads that you want to use for hifiasm with HiC assembly. This read file must be paired with the file specified by the hic2 key and this key is required if hifiasm_hic is requested by the user

### hic2
the "R2" illumina paired end HiC reads that you want to use for hifiasm with HiC assembly. This read file must be paired with the file specified by the hic1 key and this key is required if hifiasm_hic is requested by the user

### par1
the first parental reads of the trio that you want to use for hifiasm with trio assembly. This is required for running hifiasm_trio

### par2
the second parental reads of the trio that you want to use for hifiasm with trio assembly. This is required for running hifiasm_trio

### genome_size
Estimated genome size. Example: "5M" for ecoli, "3G" for human, etc. This option is required if using wtdbg2 and will be used by some other rules if provided

## Performance Related Config Keys
You can probably ignore these unless you are executing using tibanna or a cluster.

### cpu_assembly
The number of threads/processes to use when running assembly creation jobs. (miniasm, flye, wtdbg2, spades)

### cpu_polish
The number of threads/processes to use when running jobs that execute on draft assemblys. (racon, medaka, pilon, busco, racon_short)

### cpu_aux
The number of threads/processes to use when running jobs that aren't polish or assembly related.

### mem_assembly
The ammount of memory, in mebibytes, to assign to assembly creation related jobs. (miniasm, flye, wtdbg2, spades)

### mem_polish
The ammount of memory, in mebibytes, to assign when running jobs that execute on draft assemblys. (racon, medaka, pilon, busco, racon_short)

### mem_aux
The ammount of memory, in mebibytes, to assign when running jobs that aren't polish or assembly related

### profile
A profile option that controls default values for 'cpu_assembly', 'cpu_polish', 'mem_assembly', 'mem_polish'. Use one of: test, small, medium, large, extra, ultra.

### opt
A dictionary allowing for inserting flags into basically any step of the pipeline. These options allow for modifying the commands that will be run as part of assembly and using these options is dangerous as a result. I strongly reccomend testing your snakemake invocation using '--dryrun -p' if you are using these options to manually inspect the commands that get generated and make sure they fit your expectations. It is very easy to break this pipeline by specifying nonsensical 'opt' keys.
